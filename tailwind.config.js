// eslint-disable-next-line @typescript-eslint/no-var-requires
const colors = require('tailwindcss/colors');

/** @type {import('tailwindcss').TailwindConfig} */
module.exports = {
  content: ['./src/**/*.{ts,tsx}'],
  darkMode: 'class',
  theme: {
    extend: {
      colors: {
        'jfft-white': colors.gray['50'],
        'jfft-black': '#12130D',
        'jfft-pink': '#EA007F',
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            color: theme('colors.jfft-pink'),
            div: {
              backgroundColor: theme('colors.jfft-white'),
            },
            label: {
              color: colors.gray['600'],
            },
          },
        },
        dark: {
          css: {
            color: theme('colors.jfft-pink'),
            div: {
              backgroundColor: theme('colors.jfft-black'),
            },
            label: {
              color: colors.gray['300'],
            },
          },
        },
      }),
    },
  },
  variants: {
    typography: ['dark'],
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('@tailwindcss/forms'),
    require('tailwind-scrollbar-hide'),
  ],
};
