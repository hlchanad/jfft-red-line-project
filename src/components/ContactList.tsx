import classNames from 'classnames';
import { useState } from 'react';
import { Icons } from '@/assets/icons';
import { useAppDispatch, useAppSelector } from '@/store';
import { addPerson } from '@/store/contact.slice';
import {
  setDoUpload,
  setSavingStatus,
  setTimeout,
} from '@/store/storage.slice';
import { ContactListItem } from './ContactListItem';

export function ContactList({ className }: { className?: string }) {
  const people = useAppSelector((state) => state.contact.people);
  const { timeout } = useAppSelector((state) => state.storage);
  const dispatch = useAppDispatch();
  const [search, setSearch] = useState<string>('');

  const onSearch = (event) => {
    const search = event.target.value;
    setSearch(search);
  };

  const filtered =
    !search || search.trim() === ''
      ? people
      : people.filter(
          (person) =>
            person.name.toLowerCase().indexOf(search.toLowerCase()) >= 0,
        );

  return (
    <div className={classNames(className, 'flex flex-col overflow-y-hidden')}>
      <div className="relative h-16 flex justify-center items-center">
        <span className="font-bold">
          參加者 ({search && `${filtered.length}/`}
          {people.length})
        </span>
        <button
          className="inline-block absolute right-0 w-8 p-1.5 rounded hover:bg-gray-200 dark:hover:bg-gray-700"
          onClick={() => {
            dispatch(addPerson());
            dispatch(setSavingStatus('pending'));

            if (timeout) {
              window.clearTimeout(timeout);
            }

            dispatch(
              setTimeout(
                window.setTimeout(() => {
                  dispatch(setDoUpload(true));
                  dispatch(setTimeout(undefined));
                }, 5000),
              ),
            );
          }}
        >
          <Icons.Plus className="text-jfft-pink" />
        </button>
      </div>

      <input
        className="rounded px-2.5 py-1 mb-3 focus-visible:outline-0 bg-gray-200 dark:bg-gray-700"
        placeholder="尋找參加者..."
        onChange={onSearch}
      />

      <div className="grow overflow-y-scroll scrollbar-hide">
        {filtered.length ? (
          filtered.map((person, index) => (
            <ContactListItem
              key={index}
              person={person}
              target={people.find((target) => target.id === person.targetId)}
            />
          ))
        ) : (
          <div className="text-center text-gray-500">找不到參加者...</div>
        )}
      </div>
    </div>
  );
}
