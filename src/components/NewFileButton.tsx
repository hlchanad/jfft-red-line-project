import classNames from 'classnames';
import React from 'react';
import { useAppDispatch, useAppSelector } from '@/store';
import {
  setDate,
  setFileId,
  setFileName,
  setLastUploadedAt,
  setSavingStatus,
} from '@/store/storage.slice';
import { resetPeople, selectPerson } from '@/store/contact.slice';

export function NewFileButton({ className }: { className?: string }) {
  const { savingStatus } = useAppSelector((state) => state.storage);
  const dispatch = useAppDispatch();

  return (
    <button
      className={classNames(
        'font-bold hover:bg-jfft-pink hover:text-gray-200',
        'disabled:hover:bg-transparent disabled:hover:text-jfft-pink',
        className,
      )}
      disabled={savingStatus === 'saving'}
      onClick={() => {
        if (savingStatus === 'pending') {
          const isClear = confirm('有資料未儲存，確定要開新檔案？');
          if (!isClear) return;
        }

        dispatch(resetPeople());
        dispatch(selectPerson(null));
        dispatch(setSavingStatus(undefined));
        dispatch(setLastUploadedAt(undefined));
        dispatch(setFileId(undefined));
        dispatch(setFileName(undefined));
        dispatch(setDate(undefined));
      }}
    >
      新檔案
    </button>
  );
}
