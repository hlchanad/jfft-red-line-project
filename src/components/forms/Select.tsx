import classNames from 'classnames';

export function Select({
  id,
  label,
  value,
  options,
  onChange,
  className,
}: {
  id: string;
  label: string;
  value?: string;
  options: Array<{
    value: string;
    label: string;
  }>;
  onChange: (event) => void;
  className?: string;
}) {
  return (
    <label className={classNames(className, 'block mb-4')}>
      <span className="text-jfft-pink font-bold">{label}</span>
      <select
        className="block w-full mt-0 px-0.5
                   bg-transparent
                   border-0 border-b-2 border-gray-200
                   focus:ring-0 focus:border-black
                   dark:border-gray-500 dark:focus:border-gray-200
                  "
        id={id}
        value={value}
        onChange={(event) => onChange(event.target.value)}
      >
        {options.map((option) => (
          <option
            className="bg-jfft-white dark:bg-jfft-black"
            key={option.value}
            value={option.value}
          >
            {option.label}
          </option>
        ))}
      </select>
    </label>
  );
}
