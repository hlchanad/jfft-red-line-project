import classNames from 'classnames';

export function Textarea({
  id,
  label,
  value,
  placeholder,
  onChange,
  className,
  rows = 5,
}: {
  id: string;
  label: string;
  value?: string;
  placeholder?: string;
  onChange: (event) => void;
  className?: string;
  rows?: number;
}) {
  return (
    <label className={classNames(className, 'block mb-4')}>
      <span className="text-jfft-pink font-bold">{label}</span>
      <textarea
        className="mt-0 block w-full px-0.5
                   bg-transparent
                   border-0 border-b-2 border-gray-200
                   focus:ring-0 focus:border-black
                   dark:border-gray-500 dark:focus:border-gray-200
                  "
        id={id}
        value={value}
        placeholder={placeholder}
        onChange={(event) => onChange(event.target.value)}
        rows={rows}
      />
    </label>
  );
}
