import classNames from 'classnames';

export function Input({
  id,
  label,
  value = '',
  placeholder = '',
  onChange,
  className,
}: {
  id: string;
  label: string;
  value?: string;
  placeholder?: string;
  onChange: (value) => void;
  className?: string;
}) {
  return (
    <label className={classNames(className, 'block mb-4')}>
      <span className="text-jfft-pink font-bold">{label}</span>
      <input
        className="mt-0 block w-full px-0.5
                   bg-transparent
                   border-0 border-b-2 border-gray-200
                   focus:ring-0 focus:border-black
                   dark:border-gray-500 dark:focus:border-gray-200
                  "
        id={id}
        type="text"
        placeholder={placeholder}
        value={value}
        onChange={(event) => onChange(event.target.value)}
      />
    </label>
  );
}
