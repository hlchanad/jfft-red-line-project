import classNames from 'classnames';
import { ChangeEvent } from 'react';

export function Radio({
  id,
  label,
  value,
  options,
  onChange,
  className,
}: {
  id: string;
  label: string;
  value?: string;
  options: Array<{
    value: string;
    label: string;
  }>;
  onChange: (event) => void;
  className?: string;
}) {
  return (
    <label className={classNames(className, 'block mb-4')}>
      <span className="text-jfft-pink font-bold">{label}</span>
      <div
        onChange={(event: ChangeEvent<HTMLInputElement>) =>
          onChange(event.target.value)
        }
      >
        {options.map((option) => (
          <label key={option.value} className="inline-flex items-center mr-4">
            <input
              className="bg-transparent
                         text-jfft-pink focus:ring-jfft-pink
                        "
              type="radio"
              name={id}
              value={option.value}
              checked={value === option.value}
              onChange={() => null}
            />
            <span className="ml-2">{option.label}</span>
          </label>
        ))}
      </div>
    </label>
  );
}
