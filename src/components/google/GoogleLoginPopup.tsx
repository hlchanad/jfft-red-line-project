import React from 'react';
import { useAppDispatch, useAppSelector } from '@/store';
import { setShowGoogleLoginPopup } from '@/store/gapi.slice';
import { LoginButton } from './LoginButton';

export function GoogleLoginPopup() {
  const { oauthToken } = useAppSelector((state) => state.gapi);
  const dispatch = useAppDispatch();

  return (
    <div
      className="overflow-y-auto overflow-x-hidden
                 fixed top-0 right-0 left-0 z-10 w-full h-full
                 bg-jfft-black/50 dark:bg-jfft-white/50
                 flex items-center justify-center"
    >
      <div
        className="w-10/12 sm:w-1/2 p-6
                   bg-jfft-white dark:bg-jfft-black
                   border-4 border-gray-200 dark:border-gray-700 rounded-xl
                   flex flex-col items-center justify-center"
      >
        {!oauthToken && (
          <React.Fragment>
            <div className="flex items-center">
              <span className="font-bold">登入Google</span>
            </div>
            <p className="text-justify">
              即使不以Google登入，都可以使用本程式大部分功能，
              但登入後可以用Google作為儲存工具，用作自動上傳或下載檔案，
              確保你不會違失檔案資料。
            </p>
            <p className="text-justify">
              登入Google並剔選Google Drive權限，
              此程式並不會儲存任何Google帳戶的任何資料，
              只會用作顯示登入狀態以及儲存檔案。
            </p>
            <LoginButton />
            <button
              className="underline mt-3"
              onClick={() => dispatch(setShowGoogleLoginPopup(false))}
            >
              遲啲先啦...
            </button>
          </React.Fragment>
        )}
      </div>
    </div>
  );
}
