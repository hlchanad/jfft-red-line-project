import React from 'react';
import { useAppSelector } from '@/store';
import { Icons } from '@/assets/icons';

export function Profile() {
  const { imageUrl } = useAppSelector((state) => state.gapi);

  return (
    <div className="flex items-center">
      {!imageUrl && (
        <Icons.Avatar
          className="bg-gray-200 text-gray-400 dark:bg-gray-700 text-gray-500
                     rounded-full w-10 m-2
                     hover:ring-4 ring-gray-300 dark:ring-gray-600"
        />
      )}
      {imageUrl && (
        <img
          className="rounded-full w-10 m-2
                     hover:ring-4 ring-gray-300 dark:ring-gray-600"
          src={imageUrl}
          alt="user's avatar"
        />
      )}
    </div>
  );
}
