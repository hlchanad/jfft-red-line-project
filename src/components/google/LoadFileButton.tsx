import classNames from 'classnames';
import React from 'react';
import { googleService, storageService } from '@/services';
import { useAppDispatch, useAppSelector } from '@/store';
import { resetPeople, selectPerson, setPeople } from '@/store/contact.slice';
import {
  setDate,
  setFileId,
  setFileName,
  setLastUploadedAt,
  setSavingStatus,
} from '@/store/storage.slice';
import { setShowGoogleLoginPopup } from '@/store/gapi.slice';

export function LoadFileButton({ className }: { className?: string }) {
  const API_KEY = process.env.REACT_APP_GOOGLE_API_KEY as string;

  const { loadedClient, loadedPicker, oauthToken } = useAppSelector(
    (state) => state.gapi,
  );
  const { savingStatus } = useAppSelector((state) => state.storage);
  const dispatch = useAppDispatch();

  return (
    <button
      className={classNames(
        'font-bold hover:bg-jfft-pink hover:text-gray-200',
        'disabled:hover:bg-transparent disabled:hover:text-jfft-pink',
        className,
      )}
      disabled={
        !loadedClient ||
        !loadedPicker ||
        // savingStatus === 'pending' ||
        savingStatus === 'saving'
      }
      onClick={async () => {
        if (!oauthToken) {
          dispatch(setShowGoogleLoginPopup(true));
          return;
        }

        if (savingStatus === 'pending') {
          const isClear = confirm('有資料未儲存，確定要開另一個檔案？');
          if (!isClear) return;
        }

        dispatch(resetPeople());
        dispatch(selectPerson(null));
        dispatch(setSavingStatus(undefined));

        let doc: Awaited<ReturnType<typeof googleService.selectFile>>;

        try {
          doc = await googleService.selectFile({
            developerKey: API_KEY,
            oauthToken: oauthToken as string,
            mimeTypes: 'application/json',
          });
        } catch (error) {
          alert('唔知咩事揀唔到個檔');
          return;
        }

        if (!doc) return;

        const fileId = doc.id;
        const fileName = doc.name.substring(0, doc.name.lastIndexOf('.'));

        let body: Awaited<ReturnType<typeof googleService.downloadFile>>;

        try {
          body = await googleService.downloadFile({ fileId });
        } catch (error) {
          alert('唔知咩事down唔到個檔');
          return;
        }

        if (!storageService.verifyJson(body)) {
          alert('檔案不正確');
          return;
        }

        const loadedData = storageService.loadJson(body);

        dispatch(setFileId(fileId));
        dispatch(setFileName(fileName));
        dispatch(setDate(loadedData.date));
        dispatch(setPeople(loadedData.people));
        dispatch(selectPerson(null));
        dispatch(setSavingStatus(undefined));
        dispatch(setLastUploadedAt(undefined));
      }}
    >
      開啟舊檔
    </button>
  );
}
