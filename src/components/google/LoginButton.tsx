import React from 'react';
import { googleService } from '@/services';
import { useAppDispatch, useAppSelector } from '@/store';
import { login, setShowGoogleLoginPopup } from '@/store/gapi.slice';

export function LoginButton({ className }: { className?: string }) {
  const { loadedClient } = useAppSelector((state) => state.gapi);
  const dispatch = useAppDispatch();

  return (
    <button
      className={className}
      disabled={!loadedClient}
      onClick={async () => {
        await googleService.login();

        const user = googleService.currentUser();

        if (user) {
          if (!googleService.hasDriveScope(user.scopes)) {
            alert('剔返Google Drive個權限再嚟過');
            return;
          }

          dispatch(
            login({
              ...user,
              oauthToken: user.token,
            }),
          );
          dispatch(setShowGoogleLoginPopup(false));
        }
      }}
    >
      <div
        className="w-48 h-11 bg-no-repeat bg-cover bg-center
                   bg-[url('/public/google/login-btn/btn_google_signin_light_normal_web.png')]
                   focus:bg-[url('/public/google/login-btn/btn_google_signin_light_focus_web.png')]
                   hover:bg-[url('/public/google/login-btn/btn_google_signin_light_pressed_web.png')]
                   active:bg-[url('/public/google/login-btn/btn_google_signin_light_pressed_web.png')]
                   dark:bg-[url('/public/google/login-btn/btn_google_signin_dark_normal_web.png')]
                   dark:focus:bg-[url('/public/google/login-btn/btn_google_signin_dark_focus_web.png')]
                   dark:hover:bg-[url('/public/google/login-btn/btn_google_signin_dark_pressed_web.png')]
                   dark:active:bg-[url('/public/google/login-btn/btn_google_signin_dark_pressed_web.png')]
                  "
      />
    </button>
  );
}
