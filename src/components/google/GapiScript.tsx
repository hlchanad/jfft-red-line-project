import React from 'react';
import makeAsyncScriptLoader from 'react-async-script';
import { googleService } from '@/services';
import { useAppDispatch } from '@/store';
import { setLoadedClient, setLoadedPicker } from '@/store/gapi.slice';

const GapiComponent = makeAsyncScriptLoader(
  'https://apis.google.com/js/api.js',
)(() => <React.Fragment />);

export function GapiScript() {
  const CLIENT_ID = process.env.REACT_APP_GOOGLE_CLIENT_ID as string;
  const API_KEY = process.env.REACT_APP_GOOGLE_API_KEY as string;

  const dispatch = useAppDispatch();

  return (
    <GapiComponent
      asyncScriptOnLoad={() => {
        googleService.init({
          apiKey: API_KEY,
          clientId: CLIENT_ID,
          callbacks: {
            initClient: () => dispatch(setLoadedClient()),
            initPicker: () => dispatch(setLoadedPicker()),
          },
        });
      }}
    />
  );
}
