import classNames from 'classnames';
import React from 'react';
import { googleService } from '@/services';
import { useAppDispatch, useAppSelector } from '@/store';
import { logout, setShowGoogleLoginPopup } from '@/store/gapi.slice';
import { resetPeople, selectPerson } from '@/store/contact.slice';
import {
  setDate,
  setFileId,
  setFileName,
  setLastUploadedAt,
  setSavingStatus,
} from '@/store/storage.slice';

export function LogoutButton({ className }: { className?: string }) {
  const { loadedClient, oauthToken } = useAppSelector((state) => state.gapi);
  const dispatch = useAppDispatch();

  return (
    <button
      className={classNames(
        'border-2 border-jfft-pink py-2 rounded font-bold ' +
          'hover:bg-jfft-pink hover:text-gray-200',
        className,
      )}
      disabled={!loadedClient || !oauthToken}
      onClick={async () => {
        await googleService.logout();
        dispatch(logout());
        dispatch(resetPeople());
        dispatch(selectPerson(null));
        dispatch(setFileId(undefined));
        dispatch(setFileName(undefined));
        dispatch(setDate(undefined));
        dispatch(setSavingStatus(undefined));
        dispatch(setLastUploadedAt(undefined));
        dispatch(setShowGoogleLoginPopup(true));
      }}
    >
      登出
    </button>
  );
}
