import moment from 'moment';
import React from 'react';
import { Icons } from '@/assets/icons';
import { googleService, storageService } from '@/services';
import { useAppDispatch, useAppSelector } from '@/store';
import {
  setDate,
  setFileId,
  setFileName,
  setLastUploadedAt,
  setSavingStatus,
} from '@/store/storage.slice';

export function NewFilePopup({
  setShowPopup,
}: {
  setShowPopup: (showPopup: boolean) => void;
}) {
  const API_KEY = process.env.REACT_APP_GOOGLE_API_KEY as string;

  const { people } = useAppSelector((state) => state.contact);
  const { fileName } = useAppSelector((state) => state.storage);
  const { oauthToken } = useAppSelector((state) => state.gapi);
  const dispatch = useAppDispatch();

  const date = moment().format('YYYY-MM-DD');

  const createFile = async (folderId?: string) => {
    let file: Awaited<ReturnType<typeof googleService.createFile>>;

    try {
      file = await googleService.createFile({
        oauthToken: oauthToken as string,
        file: {
          name: fileName ? `${fileName}.json` : `jfft-red-line-${date}.json`,
          content: storageService.exportData({ date, people }),
          mimeType: 'application/json',
        },
        folderId,
      });
    } catch (error) {
      alert('唔知咩事開唔到新檔');
      return;
    }

    const fileId = file?.id;

    if (!fileId) {
      alert('開唔到檔案，試吓撳多次');
      return;
    }

    dispatch(setFileId(fileId));
    if (!fileName) {
      dispatch(setFileName(`jfft-red-line-${date}`));
    }
    dispatch(setDate(date));
    dispatch(setSavingStatus('saved'));
    dispatch(setLastUploadedAt(new Date()));
    setShowPopup(false);
  };

  return (
    <div
      className="overflow-y-auto overflow-x-hidden
                 fixed top-0 right-0 left-0 z-20 w-full h-full
                 bg-jfft-black/50 dark:bg-jfft-white/50
                 flex items-center justify-center"
    >
      <div
        className="w-10/12 sm:w-1/2 p-6
                   bg-jfft-white dark:bg-jfft-black
                   border-4 border-gray-200 dark:border-gray-700 rounded-xl
                   flex flex-col items-center justify-center relative"
      >
        <button
          className="inline-block absolute right-0 top-0 w-8 p-1.5
                     rounded hover:bg-gray-200 dark:hover:bg-gray-700"
          onClick={() => setShowPopup(false)}
        >
          <Icons.Times className="text-jfft-pink" />
        </button>
        <p className="font-bold mt-0">儲存喺最出面？</p>
        <div className="bg-transparent">
          <button
            className="border-2 border-jfft-pink px-5 py-2 mr-3 rounded font-bold
                       hover:bg-jfft-pink hover:text-gray-200"
            onClick={() => createFile()}
          >
            是
          </button>
          <button
            className="border-2 border-jfft-pink px-5 py-2 mr-3 rounded font-bold
                       hover:bg-jfft-pink hover:text-gray-200"
            onClick={async () => {
              let folder: Awaited<
                ReturnType<typeof googleService.selectFolder>
              >;

              try {
                folder = await googleService.selectFolder({
                  developerKey: API_KEY,
                  oauthToken: oauthToken as string,
                });
              } catch (error) {
                alert('唔知咩事揀唔到個folder');
                return;
              }

              const folderId = folder?.id;

              if (!folderId) {
                alert('揀唔到資料夾，試吓撳多次');
                return;
              }

              await createFile(folderId);
            }}
          >
            不，揀資料夾
          </button>
        </div>
      </div>
    </div>
  );
}
