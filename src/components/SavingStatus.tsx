import classNames from 'classnames';
import moment from 'moment';
import React, { useState } from 'react';
import { Icons } from '@/assets/icons';
import { useAppDispatch, useAppSelector } from '@/store';
import { setShowGoogleLoginPopup } from '@/store/gapi.slice';
import { NewFilePopup } from './google/NewFilePopup';

export function SavingStatus({ className }: { className?: string }) {
  const { fileId, savingStatus, lastUploadedAt } = useAppSelector(
    (state) => state.storage,
  );
  const { oauthToken } = useAppSelector((state) => state.gapi);
  const dispatch = useAppDispatch();

  const [showPopup, setShowPopup] = useState(false);

  const getSavingStatus = () => {
    if (savingStatus === 'saving') {
      return '儲存中...';
    } else if (savingStatus === 'pending') {
      if (fileId) {
        return '有改變未儲存，稍後將自動儲存';
      } else {
        return '有改變未儲存，按此以儲存';
      }
    } else if (lastUploadedAt !== undefined) {
      return `已於${moment(lastUploadedAt).format('HH:mm')}儲存`;
    } else {
      return '';
    }
  };

  return (
    <React.Fragment>
      <div
        className={classNames(
          {
            'bg-jfft-pink text-white ml-1.5 px-1.5 font-bold rounded':
              savingStatus === 'pending',
            'cursor-pointer hover:bg-pink-800':
              savingStatus === 'pending' && !fileId,
          },
          className,
        )}
        onClick={() => {
          if (savingStatus !== 'pending') return;

          if (!oauthToken) {
            dispatch(setShowGoogleLoginPopup(true));
          } else if (!fileId) {
            setShowPopup(true);
          }
        }}
      >
        {savingStatus === 'pending' && (
          <Icons.Exclamation className="w-5 mr-0.5 inline" />
        )}
        {getSavingStatus()}
      </div>
      {showPopup && <NewFilePopup setShowPopup={setShowPopup} />}
    </React.Fragment>
  );
}
