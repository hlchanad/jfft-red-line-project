import React from 'react';
import { Icons } from '@/assets/icons';
import { useTheme } from '../providers/theme.provider';

export function ThemeSelector() {
  const { theme, setTheme } = useTheme();
  return (
    <div className="flex items-center">
      <button
        className="w-8 p-1 hover:bg-gray-200 dark:hover:bg-gray-700 hover:rounded"
        onClick={() => setTheme('default')}
      >
        {theme === 'default' ? (
          <Icons.Sun className="text-jfft-pink" />
        ) : (
          <Icons.SunOutline className="text-jfft-pink" />
        )}
      </button>
      <span className="mx-1.5">/</span>
      <button
        className="w-8 p-1 hover:bg-gray-200 dark:hover:bg-gray-700 hover:rounded"
        onClick={() => setTheme('dark')}
      >
        {theme === 'dark' ? (
          <Icons.Moon className="text-jfft-pink" />
        ) : (
          <Icons.MoonOutline className="text-jfft-pink" />
        )}
      </button>
    </div>
  );
}
