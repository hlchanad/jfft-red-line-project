import classNames from 'classnames';
import { useAppDispatch } from '@/store';
import { selectPerson } from '@/store/contact.slice';
import { Person } from '../types';

export function ContactListItem({
  className,
  person,
  target,
}: {
  className?: string;
  person: Person;
  target?: Person | undefined;
}) {
  const dispatch = useAppDispatch();

  return (
    <div
      className={classNames(
        className,
        'flex items-center mb-3 cursor-pointer rounded hover:bg-gray-200 dark:hover:bg-gray-700',
      )}
      onClick={() => dispatch(selectPerson(person))}
    >
      <div className="not-prose h-20 w-20 flex-none rounded mr-3">
        {person.images.length > 0 ? (
          <img
            className="h-full w-full object-cover rounded"
            src={person.images[0].base64}
            alt="avatar thumbnail"
          />
        ) : (
          <div
            className="w-full h-full rounded bg-gray-200 dark:bg-gray-700
                       flex flex-col justify-center items-center
                      "
          >
            <div>Hello</div>
            <div>無圖</div>
          </div>
        )}
      </div>
      <div className="grow flex flex-col bg-transparent">
        <div className="font-bold bg-transparent">
          {person.name === '' ? '未命名參加者' : person.name}
        </div>
        <div className="bg-transparent">
          <label className="text-xs mr-2">想識:</label>
          <span>{target?.name}</span>
        </div>
      </div>
    </div>
  );
}
