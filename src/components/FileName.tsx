import classNames from 'classnames';
import React from 'react';
import { useAppDispatch, useAppSelector } from '@/store';
import { setDoUpload, setFileName, setTimeout } from '@/store/storage.slice';

export function FileName({ className }: { className?: string }) {
  const { fileName, timeout } = useAppSelector((state) => state.storage);
  const dispatch = useAppDispatch();

  const onEditFileName = async (fileName: string) => {
    dispatch(setFileName(fileName));

    if (timeout) {
      window.clearTimeout(timeout);
    }

    dispatch(
      setTimeout(
        window.setTimeout(() => {
          dispatch(setDoUpload(true));
          dispatch(setTimeout(undefined));
        }, 5000),
      ),
    );
  };

  return (
    <div className={classNames(className)}>
      <input
        className="mt-0 block px-3 font-bold
                   bg-transparent border-none focus:ring-0
                   hover:bg-gray-200 dark:hover:bg-gray-700
                  "
        id="filename"
        type="text"
        placeholder="檔案名稱"
        value={fileName ?? ''}
        size={fileName?.length ?? 50}
        onChange={(event) => onEditFileName(event.target.value)}
      />
    </div>
  );
}
