import classNames from 'classnames';
import JfftIcon from '@/assets/jfft-icon.png';
import { useAppDispatch, useAppSelector } from '@/store';
import { FileName } from './FileName';
import { SavingStatus } from './SavingStatus';
import { ThemeSelector } from './ThemeSelector';
import { LoadFileButton } from './google/LoadFileButton';
import { LogoutButton } from './google/LogoutButton';
// import { NewFileButton } from './google/NewFileButton';
import { Profile } from './google/Profile';
import { setShowGoogleLoginPopup } from '@/store/gapi.slice';
import { NewFileButton } from './NewFileButton';

export function Header({ className }: { className?: string }) {
  const { oauthToken } = useAppSelector((state) => state.gapi);
  const dispatch = useAppDispatch();
  return (
    <div
      className={classNames(
        'flex border-b-[1px] border-gray-200 dark:border-gray-700',
        className,
      )}
    >
      <img className="w-12 m-3 object-contain" src={JfftIcon} alt="logo" />
      <div className="grow flex flex-col">
        <FileName className="mt-1.5" />
        <div className="flex items-center">
          <NewFileButton className="px-3 py-1 border-r-[1px] border-gray-200 dark:border-gray-700" />
          <LoadFileButton className="px-3 py-1 border-r-[1px] border-gray-200 dark:border-gray-700" />
          <SavingStatus className="px-3" />
        </div>
      </div>
      <ThemeSelector />
      <Profile />
      {oauthToken && <LogoutButton className="border-none px-3 my-auto" />}
      {!oauthToken && (
        <div
          className="px-3 py-2 rounded my-auto font-bold cursor-pointer
                     hover:bg-gray-200 dark:hover:bg-gray-700"
          onClick={() => dispatch(setShowGoogleLoginPopup(true))}
        >
          登入 Google
        </div>
      )}
    </div>
  );
}
