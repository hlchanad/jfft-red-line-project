import classNames from 'classnames';
import React, { useEffect, useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { useAppDispatch, useAppSelector } from '@/store';
import {
  deletePerson,
  selectPerson,
  updatePerson,
} from '@/store/contact.slice';
import {
  setDoUpload,
  setSavingStatus,
  setTimeout,
} from '@/store/storage.slice';
import { Input, Radio, Select, Textarea } from './forms';
import './ContactDetail.css';
import { Person } from '../types';
import { fileToBase64 } from '@/lib';
import { Icons } from '@/assets/icons';

export function ContactDetail({ className }: { className?: string }) {
  const people = useAppSelector((state) => state.contact.people);
  const person = useAppSelector((state) => state.contact.selectedPerson);
  const { timeout } = useAppSelector((state) => state.storage);
  const [carouselIndex, setCarouselIndex] = useState<number>(0);
  const dispatch = useAppDispatch();

  const targetedBy = people.filter(
    (targetBy) => targetBy.targetId === person?.id,
  );

  useEffect(() => {
    const thumbsWrappers = document.getElementsByClassName('thumbs-wrapper');

    if (thumbsWrappers?.[0]) {
      const element = thumbsWrappers[0];

      if (!element.classList.contains('scrollbar-hide')) {
        element.classList.add('scrollbar-hide');
      }
    }
  });

  const delayUpload = () => {
    if (timeout) {
      window.clearTimeout(timeout);
    }

    dispatch(
      setTimeout(
        window.setTimeout(() => {
          dispatch(setDoUpload(true));
          dispatch(setTimeout(undefined));
        }, 5000),
      ),
    );
  };

  const onEditFields = async (
    updates: Partial<Omit<Person, 'id'>>,
    upload = true,
  ) => {
    dispatch(updatePerson(updates));
    dispatch(setSavingStatus('pending'));

    if (upload) {
      delayUpload();
    }
  };

  const { getRootProps, getInputProps } = useDropzone({
    noClick: true,
    onDropAccepted: async (acceptedFiles: File[]) => {
      const base64Images = await Promise.all(
        acceptedFiles.map(async (file) => ({
          base64: await fileToBase64(file),
        })),
      );

      await onEditFields({
        images: [...(person?.images ?? []), ...base64Images],
      });
    },
  });

  if (
    person?.images &&
    person.images.length > 0 &&
    carouselIndex >= person.images.length
  ) {
    setCarouselIndex(0);
  }

  const sexLabel = (sex: 'M' | 'F') => (sex === 'M' ? '男' : '女');

  return (
    <div
      className={classNames(
        className,
        'overflow-y-scroll scrollbar-hide py-3 flex flex-wrap content-start',
      )}
    >
      {!person ? (
        <div className="grow mt-3 mr-3 text-center text-gray-500">
          未選擇參加者...
        </div>
      ) : (
        <React.Fragment>
          <div className="grow flex flex-col mr-3">
            <Input
              className="w-full"
              id="name"
              label="名稱"
              placeholder="未命名參加者"
              value={person?.name}
              onChange={(name) => onEditFields({ name })}
            />

            <Input
              className="w-full"
              id="skypeId"
              label="Skype ID"
              placeholder="Skype ID"
              value={person?.skypeId}
              onChange={(skypeId) => onEditFields({ skypeId })}
            />

            <Radio
              className="w-full"
              id="sex"
              label="性別"
              value={person?.sex}
              onChange={(sex) => onEditFields({ sex })}
              options={[
                { value: 'M', label: '男' },
                { value: 'F', label: '女' },
              ]}
            />

            <Select
              className="w-full"
              id="targetId"
              label="想識"
              value={person?.targetId ?? undefined}
              onChange={(targetId) => onEditFields({ targetId })}
              options={[
                { value: '', label: '留個名先啦' },
                ...people
                  .filter((target) => target.id !== person?.id)
                  .filter((target) => target.sex === 'F')
                  .map((target) => ({
                    value: target.id,
                    label: `[${sexLabel(target.sex)}] ${target.name}`,
                  })),
                ...people
                  .filter((target) => target.id !== person?.id)
                  .filter((target) => target.sex === 'M')
                  .map((target) => ({
                    value: target.id,
                    label: `[${sexLabel(target.sex)}] ${target.name}`,
                  })),
              ]}
            />

            <label className="w-full block mb-4">
              <span className="text-jfft-pink font-bold">
                {targetedBy.length <= 0 ? (
                  <React.Fragment>無人想識 {person?.name} ...</React.Fragment>
                ) : (
                  <React.Fragment>呢啲人想識 {person?.name} :</React.Fragment>
                )}
              </span>
              <div className="block">
                {targetedBy.map((person, index) => (
                  <React.Fragment key={index}>
                    <span
                      className="cursor-pointer hover:text-jfft-pink"
                      onClick={() => dispatch(selectPerson(person))}
                    >
                      {person?.name}
                    </span>
                    {targetedBy.length - 1 > index ? <span>、</span> : ''}
                  </React.Fragment>
                ))}
              </div>
            </label>

            <Textarea
              className="w-full"
              id="remarks"
              label="備註"
              rows={10}
              placeholder="備註欄"
              value={person?.remarks}
              onChange={(remarks) => onEditFields({ remarks })}
            />
            <button
              className="px-8 py-2 rounded-lg font-bold
                         bg-jfft-pink text-jfft-white hover:bg-pink-800 active:bg-pink-800"
              disabled={!person}
              onClick={() => {
                dispatch(deletePerson(person.id));
                dispatch(setSavingStatus('pending'));
                delayUpload();
              }}
            >
              刪除
            </button>
          </div>
          <div className="w-5/12 cursor-default" {...getRootProps()}>
            <input {...getInputProps()} />

            {(person?.images ?? []).length <= 0 ? (
              <React.Fragment>
                <div className="text-center">Hello 無圖 ?</div>
                <label className="block text-center my-1">
                  <span>拉啲圖入嚟...</span>
                  <br />
                  <span className="text-gray-500 text-sm">(可以拉多過1張)</span>
                </label>
              </React.Fragment>
            ) : (
              <Carousel
                className="not-prose"
                showStatus={false}
                selectedItem={carouselIndex}
                onChange={(index) => setCarouselIndex(index)}
                renderItem={(item) =>
                  (item as React.ReactElement)?.props.children
                }
                renderThumbs={(children) =>
                  children.map((child, index) => (
                    <React.Fragment key={index}>
                      <div className="relative h-full">
                        <button
                          className="absolute right-0 top-0 w-5 rounded
                                       bg-gray-700/50 hover:bg-gray-700/75"
                          onClick={() => {
                            const images = [...person?.images];
                            images.splice(index, 1);

                            onEditFields({ images });
                          }}
                        >
                          <Icons.Times className="text-jfft-pink" />
                        </button>
                        {(child as React.ReactElement).props.children}
                      </div>
                    </React.Fragment>
                  ))
                }
                renderIndicator={(clickHandler, isSelected, index) => (
                  <div
                    key={index}
                    className={classNames(
                      'cursor-pointer inline-block mx-1 w-3 h-3 rounded-full opacity-100',
                      {
                        'bg-gray-200 dark:bg-gray-700 hover:bg-jfft-pink dark:hover:bg-jfft-pink':
                          !isSelected,
                        'bg-jfft-pink': isSelected,
                      },
                    )}
                    onClick={clickHandler}
                  />
                )}
              >
                {(person?.images ?? []).map((file, index) => (
                  <div key={index}>
                    <img src={file.base64} alt="image" />
                  </div>
                ))}
              </Carousel>
            )}
          </div>
        </React.Fragment>
      )}
    </div>
  );
}
