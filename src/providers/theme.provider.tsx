import classNames from 'classnames';
import React from 'react';
import { StateProviderFactory } from '@/lib';

type AllowedTheme = 'default' | 'dark';

const Theme = StateProviderFactory({ defaultState: 'dark' });

function Content({ children }) {
  const { state } = Theme.useState();
  return (
    <div
      className={classNames('prose min-w-full', {
        [`prose-dark`]: state === 'dark',
        [`dark`]: state === 'dark',
      })}
    >
      {children}
    </div>
  );
}

export function ThemeProvider({ children }) {
  return (
    <Theme.Provider>
      <Content>{children}</Content>
    </Theme.Provider>
  );
}

export function useTheme() {
  const { state, setState } = Theme.useState();
  return {
    theme: state,
    setTheme: (theme: AllowedTheme) => setState(theme),
  };
}
