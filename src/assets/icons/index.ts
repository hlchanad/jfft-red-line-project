import { ReactComponent as Avatar } from './avatar.svg';
import { ReactComponent as Exclamation } from './exclamation.svg';
import { ReactComponent as Sun } from './sun.svg';
import { ReactComponent as SunOutline } from './sun-outline.svg';
import { ReactComponent as Plus } from './plus.svg';
import { ReactComponent as Moon } from './moon.svg';
import { ReactComponent as MoonOutline } from './moon-outline.svg';
import { ReactComponent as Times } from './times.svg';

export const Icons = {
  Avatar,
  Exclamation,
  Moon,
  MoonOutline,
  Plus,
  Sun,
  SunOutline,
  Times,
};
