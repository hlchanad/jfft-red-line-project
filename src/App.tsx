import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ContactRoute } from './routes/ContactRoute';
import { PrivacyRoute } from './routes/PrivacyRoute';
import { HomeRoute } from './routes/HomeRoute';

function App() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomeRoute />} />
          <Route path="/app" element={<ContactRoute />} />
          <Route path="/privacy" element={<PrivacyRoute />} />
        </Routes>
      </BrowserRouter>
    </React.Fragment>
  );
}

export default App;
