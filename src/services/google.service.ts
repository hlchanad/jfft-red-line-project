export async function init(options: {
  apiKey: string;
  clientId: string;
  callbacks?: {
    initClient?: () => void;
    initPicker?: () => void;
  };
}) {
  return Promise.all([
    new Promise<void>((resolve) => {
      window.gapi.load('client:auth2', async () => {
        await window.gapi.client.init({
          apiKey: options.apiKey,
          clientId: options.clientId,
          discoveryDocs: [
            'https://www.googleapis.com/discovery/v1/apis/drive/v3/rest',
          ],
          scope: 'https://www.googleapis.com/auth/drive.file',
        });

        options.callbacks?.initClient?.();

        resolve();
      });
    }),
    new Promise<void>((resolve) => {
      window.gapi.load('picker', async () => {
        options.callbacks?.initPicker?.();

        resolve();
      });
    }),
  ]);
}

export async function login(): Promise<{
  token: string;
}> {
  const googleUser = await window.gapi.auth2.getAuthInstance().signIn();
  const token = googleUser.getAuthResponse().access_token;

  return { token };
}

export async function logout() {
  return window.gapi.auth2.getAuthInstance().disconnect();
}

export function currentUser(): {
  name: string;
  imageUrl: string;
  token: string;
  scopes: Array<string>;
} | null {
  const googleUser = window.gapi.auth2.getAuthInstance().currentUser.get();

  if (!googleUser || !googleUser.getBasicProfile()) {
    return null;
  }

  const scopes = window.gapi.auth2
    .getAuthInstance()
    .currentUser.get()
    .getGrantedScopes()
    .split(' ');

  const token = googleUser.getAuthResponse().access_token;
  const profile = googleUser.getBasicProfile();
  const name = profile.getName();
  const imageUrl = profile.getImageUrl();

  return { name, imageUrl, token, scopes };
}

export function hasDriveScope(scopes: Array<string>): boolean {
  return scopes.includes('https://www.googleapis.com/auth/drive.file');
}

async function createPicker(options: {
  developerKey: string;
  oauthToken: string;
  title?: string;
  view: Parameters<typeof google.picker.PickerBuilder.prototype.addView>[0];
}) {
  return new Promise<google.picker.DocumentObject | null>((resolve) => {
    const picker = new window.google.picker.PickerBuilder()
      .setDeveloperKey(options.developerKey)
      .setOAuthToken(options.oauthToken)
      .addView(options.view)
      .setLocale('zh-HK')
      .setCallback((data) => {
        const action = data[window.google.picker.Response.ACTION];

        if (action === window.google.picker.Action.PICKED) {
          resolve(data?.[window.google.picker.Response.DOCUMENTS]?.[0]);
        } else if (action === window.google.picker.Action.CANCEL) {
          resolve(null);
        }
      });

    if (options.title) {
      picker.setTitle(options.title);
    }

    picker.build().setVisible(true);
  });
}

export async function selectFile(options: {
  developerKey: string;
  oauthToken: string;
  mimeTypes?: string;
}): Promise<google.picker.DocumentObject | null> {
  const view = new window.google.picker.DocsView(
    window.google.picker.ViewId.DOCS,
  );

  if (options.mimeTypes) {
    view.setMimeTypes(options.mimeTypes);
  }

  return createPicker({
    developerKey: options.developerKey,
    oauthToken: options.oauthToken,
    view,
    title: '開啟舊檔',
  });
}

export async function selectFolder(options: {
  developerKey: string;
  oauthToken: string;
}) {
  const view = new window.google.picker.DocsView(
    window.google.picker.ViewId.FOLDERS,
  ).setSelectFolderEnabled(true);

  return createPicker({
    developerKey: options.developerKey,
    oauthToken: options.oauthToken,
    view,
    title: '選擇存檔位置',
  });
}

export async function downloadFile(options: {
  fileId: string;
}): Promise<string> {
  const response = await window.gapi.client.drive.files.get({
    fileId: options.fileId,
    alt: 'media',
  });

  return response.body;
}

export async function createFile(options: {
  oauthToken: string;
  file: {
    name: string;
    content: string;
    mimeType: string;
  };
  folderId?: string;
}): Promise<{ id: string }> {
  const form = new FormData();
  form.append(
    'metadata',
    new Blob(
      [
        JSON.stringify({
          name: options.file.name,
          mimeType: options.file.mimeType,
          ...(options.folderId && { parents: [options.folderId] }),
        }),
      ],
      { type: 'application/json' },
    ),
  );
  form.append(
    'file',
    new Blob([options.file.content], { type: options.file.mimeType }),
  );

  return new Promise<{ id: string }>((resolve) => {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
      if (xhr.readyState !== XMLHttpRequest.DONE) {
        return;
      }

      resolve(xhr.response);
    };
    xhr.open(
      'POST',
      'https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart',
    );
    xhr.setRequestHeader('Authorization', `Bearer ${options.oauthToken}`);
    xhr.send(form);
  });
}

export async function updateFile(options: {
  oauthToken: string;
  file: {
    id: string;
    content: string;
    mimeType: string;
  };
}) {
  return new Promise((resolve) => {
    const xhr = new XMLHttpRequest();
    xhr.responseType = 'json';
    xhr.onreadystatechange = function () {
      if (xhr.readyState !== XMLHttpRequest.DONE) {
        return;
      }

      resolve(xhr.response);
    };
    xhr.open(
      'PATCH',
      `https://www.googleapis.com/upload/drive/v3/files/${options.file.id}?uploadType=media`,
    );
    xhr.setRequestHeader('Authorization', `Bearer ${options.oauthToken}`);
    xhr.send(new Blob([options.file.content], { type: options.file.mimeType }));
  });
}

export async function updateFileName(options: {
  oauthToken: string;
  file: {
    id: string;
    name: string;
  };
}) {
  return new Promise((resolve) => {
    window.gapi.client.drive.files
      .update(
        {
          oauth_token: options.oauthToken,
          fileId: options.file.id,
        },
        {
          name: options.file.name,
        },
      )
      .execute((response) => resolve(response));
  });
}
