import Ajv from 'ajv';
import { Person } from '../types';
import V1Schema from '../json-schemas/export-v1.json';

export function exportData(args: {
  date: string;
  people: Array<Person>;
}): string {
  return JSON.stringify({
    app: 'jfft-red-line',
    version: 'v1',
    date: args.date,
    data: {
      people: args.people.map((person) => ({
        ...person,
        targetId: person.targetId ?? null,
      })),
    },
  });
}

export function verifyJson(data: any): boolean {
  if (typeof data !== 'string') return false;

  let object;
  try {
    object = JSON.parse(data);
  } catch (error) {
    return false;
  }

  return new Ajv().compile(V1Schema)(object);
}

export function loadJson(data: string): {
  version: string;
  date: string;
  people: Array<Person>;
} {
  const object = JSON.parse(data);

  return {
    version: object.version,
    date: object.date,
    people: object.data.people,
  };
}
