import React, { useEffect } from 'react';
import { googleService } from '@/services';
import { useAppDispatch, useAppSelector } from '@/store';
import { login } from '@/store/gapi.slice';
import { GapiScript } from '../components/google/GapiScript';
import { Header } from '../components/Header';
import { GoogleLoginPopup } from '../components/google/GoogleLoginPopup';
import { ContactBook } from '../layouts/ContactBook';

export function ContactRoute() {
  const { loadedClient, loadedPicker, showGoogleLoginPopup, oauthToken } =
    useAppSelector((state) => state.gapi);
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (loadedClient && loadedPicker && !oauthToken) {
      const user = googleService.currentUser();

      if (user) {
        dispatch(
          login({
            ...user,
            oauthToken: user.token,
          }),
        );
      }
    }
  }, [loadedClient, loadedPicker]);

  return (
    <React.Fragment>
      <GapiScript />
      <div className="h-screen flex flex-col bg-jfft-white dark:bg-jfft-black">
        <Header />
        <ContactBook className="grow" />
      </div>
      {showGoogleLoginPopup && !oauthToken && <GoogleLoginPopup />}
    </React.Fragment>
  );
}
