import React from 'react';
import { ThemeSelector } from '../components/ThemeSelector';
import JfftIcon from '@/assets/jfft-icon.png';
import { Link } from 'react-router-dom';

export function PrivacyRoute() {
  return (
    <div className="flex justify-center">
      <div
        className="w-screen sm:w-2/3 h-screen p-4 overflow-y-scroll scrollbar-hide
                   shadow-xl shadow-gray-500
                   relative flex flex-col items-center"
      >
        <div className="absolute right-0 top-0 m-3">
          <ThemeSelector />
        </div>

        <img className="w-20 m-3 object-contain" src={JfftIcon} alt="logo" />
        <h1 className="font-bold text-xl text-jfft-pink">Privacy Policy</h1>
        <div className="w-full border-t border-gray-500 my-4" />

        <h2 className="font-bold text-lg text-jfft-pink my-2 w-full text-left">
          Without Google Login
        </h2>
        <ul className="marker:text-jfft-pink mb-4 w-full text-left">
          <li>
            All contact info of participants will not be saved to any servers
          </li>
          <li>
            All the data collected are not sending to other servers by any means
          </li>
          <li>
            All the data collected are only used within the web application
          </li>
        </ul>

        <h2 className="font-bold text-lg text-jfft-pink my-2 w-full text-left">
          With Google Login
        </h2>
        <ul className="marker:text-jfft-pink mb-4 w-full text-left">
          <li>
            All contact info of participants will not be saved to any other
            servers except Google Drive
          </li>
          <li>
            The app will save and load app files using Google Drive as an
            personal storage
          </li>
          <li>An Google OAuth Login with Google Drive scopes are required</li>
          <li>The user's avatar will be used as indicator for login status</li>
          <li>
            The OAuth access token will be used for transferring app files
            between website and Google Drive
          </li>
          <li>
            All the data collected are not sending to other servers by any means
          </li>
          <li>
            All the data collected are only used within the web application
          </li>
        </ul>

        <div className="flex-grow" />
        <div>
          <Link to="/" className="underline text-jfft-pink">
            回到首頁
          </Link>
        </div>
      </div>
    </div>
  );
}
