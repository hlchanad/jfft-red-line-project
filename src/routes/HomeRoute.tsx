import React from 'react';
import JfftIcon from '@/assets/jfft-icon.png';
import { Link } from 'react-router-dom';
import { ThemeSelector } from '../components/ThemeSelector';

export function HomeRoute() {
  return (
    <div className="flex justify-center">
      <div
        className="w-screen sm:w-2/3 h-screen p-4 overflow-y-scroll scrollbar-hide
                   shadow-xl shadow-gray-500
                   relative flex flex-col items-center"
      >
        <div className="absolute right-0 top-0 m-3">
          <ThemeSelector />
        </div>

        <img className="w-20 m-3 object-contain" src={JfftIcon} alt="logo" />
        <h1 className="font-bold text-xl text-jfft-pink">JFFT 紅線計劃</h1>
        <div className="w-full border-t border-gray-500 my-4" />
        <p className="text-justify">
          呢個係一個fans為咗床狗嘅
          <a
            href="https://www.youtube.com/c/JFFLiveChannel"
            className="underline text-jfft-pink"
          >
            紅線計劃
          </a>
          節目而做嘅應用程式。記錄低唔同參加者嘅資料，方便快速搵返參加者出嚟。
        </p>
        <p className="text-justify">
          本應用程式可以用Google登入 (但不強制)，並要求使用Google Drive的權限，
          以Google Drive作儲存工具 (上傳或下載)， 日後可以讀取Google
          Drive的檔案供顯示。本程式並不會儲存任何用戶資料例如名稱或電郵等，
          只會作顯示登入狀態之用途。亦不會儲存任何參加者資料。
        </p>
        <p className="text-justify">
          即使不以Google登入，都可以使用本程式大部分功能，
          但登入後可以用Google作為儲存工具，用作自動上傳或下載檔案，
          確保你不會違失檔案資料。
        </p>
        <Link
          to="/app"
          className="py-2 px-4 my-3
                     bg-jfft-pink text-white rounded-lg font-bold
                     hover:bg-pink-800"
        >
          進入程式
        </Link>
        <div className="flex-grow" />
        <div>
          <Link to="/privacy" className="underline text-jfft-pink">
            私隱政策
          </Link>
        </div>
      </div>
    </div>
  );
}
