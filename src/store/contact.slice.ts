import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Person } from '../types';
import { nanoid } from 'nanoid';

const contactSlice = createSlice({
  name: 'contact-slice',
  initialState: {
    people: [] as Array<Person>,
    selectedPerson: null as Person | null,
  },
  reducers: {
    addPerson: (state) => {
      const newPerson: Person = {
        id: nanoid(),
        name: '',
        skypeId: '',
        sex: 'M',
        targetId: '',
        remarks: '',
        images: [],
      };

      state.people.push(newPerson);
      state.selectedPerson = newPerson;
    },
    updatePerson: (
      state,
      action: PayloadAction<Partial<Omit<Person, 'id'>>>,
    ) => {
      const index = state.people.findIndex(
        (person) => person.id === state.selectedPerson?.id,
      );

      Object.entries(action.payload).forEach(([key, value]) => {
        if (!state.selectedPerson) return;

        state.selectedPerson[key] = value;
        state.people[index][key] = value;
      });
    },
    deletePerson: (state, action: PayloadAction<string>) => {
      const index = state.people.findIndex(
        (people) => people.id === action.payload,
      );

      if (index >= 0) {
        state.people.splice(index, 1);
        state.selectedPerson = null;
      }
    },
    resetPeople: (state) => {
      state.people = [];
      state.selectedPerson = null;
    },
    selectPerson: (state, action: PayloadAction<Person | null>) => {
      state.selectedPerson = action.payload;
    },
    setPeople: (state, { payload }: PayloadAction<Array<Person>>) => {
      state.people = payload;
    },
  },
});

export const {
  addPerson,
  updatePerson,
  deletePerson,
  resetPeople,
  selectPerson,
  setPeople,
} = contactSlice.actions;

export default contactSlice.reducer;
