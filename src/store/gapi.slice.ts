import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const gapiSlice = createSlice({
  name: 'gapi-slice',
  initialState: {
    loadedClient: false,
    loadedPicker: false,
    showGoogleLoginPopup: true,
    oauthToken: undefined as string | undefined,
    name: undefined as string | undefined,
    imageUrl: undefined as string | undefined,
  },
  reducers: {
    setLoadedClient: (state) => {
      state.loadedClient = true;
    },
    setLoadedPicker: (state) => {
      state.loadedPicker = true;
    },
    setShowGoogleLoginPopup: (state, { payload }: PayloadAction<boolean>) => {
      state.showGoogleLoginPopup = payload;
    },
    login: (
      state,
      {
        payload,
      }: PayloadAction<{
        oauthToken: string;
        name: string;
        imageUrl: string;
      }>,
    ) => {
      state.oauthToken = payload.oauthToken;
      state.name = payload.name;
      state.imageUrl = payload.imageUrl;
    },
    logout: (state) => {
      state.oauthToken = undefined;
      state.name = undefined;
      state.imageUrl = undefined;
    },
  },
});

export const {
  setLoadedClient,
  setLoadedPicker,
  setShowGoogleLoginPopup,
  login,
  logout,
} = gapiSlice.actions;

export default gapiSlice.reducer;
