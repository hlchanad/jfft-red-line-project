import moment from 'moment';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type SavingStatus = undefined | 'pending' | 'saving' | 'saved';

const storageSlice = createSlice({
  name: 'storage-slice',
  initialState: {
    date: moment().format('YYYY-MM-DD') as string | undefined,
    fileId: undefined as string | undefined,
    fileName: undefined as string | undefined,
    doUpload: false,
    timeout: undefined as number | undefined,
    savingStatus: undefined as SavingStatus,
    lastUploadedAt: undefined as Date | undefined,
  },
  reducers: {
    setDate: (state, { payload }: PayloadAction<string | undefined>) => {
      state.date = payload !== undefined ? payload : undefined;
    },
    setFileId: (state, { payload }: PayloadAction<string | undefined>) => {
      state.fileId = payload;
    },
    setFileName: (state, { payload }: PayloadAction<string | undefined>) => {
      state.fileName = payload;
    },
    setSavingStatus: (state, { payload }: PayloadAction<SavingStatus>) => {
      state.savingStatus = payload;
    },
    setLastUploadedAt: (
      state,
      { payload }: PayloadAction<Date | undefined>,
    ) => {
      state.lastUploadedAt = payload;
    },
    setDoUpload: (state, { payload }: PayloadAction<boolean>) => {
      state.doUpload = payload;
    },
    setTimeout: (state, { payload }: PayloadAction<number | undefined>) => {
      state.timeout = payload;
    },
  },
});

export const {
  setDate,
  setFileId,
  setFileName,
  setSavingStatus,
  setLastUploadedAt,
  setDoUpload,
  setTimeout,
} = storageSlice.actions;

export default storageSlice.reducer;
