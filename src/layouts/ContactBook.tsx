import classNames from 'classnames';
import { useEffect } from 'react';
import { googleService, storageService } from '@/services';
import { useAppDispatch, useAppSelector } from '@/store';
import {
  setDoUpload,
  setLastUploadedAt,
  setSavingStatus,
} from '@/store/storage.slice';
import { ContactDetail } from '../components/ContactDetail';
import { ContactList } from '../components/ContactList';

export function ContactBook({ className }: { className?: string }) {
  const { people } = useAppSelector((state) => state.contact);
  const { oauthToken } = useAppSelector((state) => state.gapi);
  const { fileId, fileName, doUpload, date } = useAppSelector(
    (state) => state.storage,
  );
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (doUpload) {
      if (oauthToken && fileId) {
        dispatch(setSavingStatus('saving'));

        Promise.all([
          googleService.updateFile({
            oauthToken: oauthToken as string,
            file: {
              id: fileId as string,
              content: storageService.exportData({
                date: date as string,
                people,
              }),
              mimeType: 'application/json',
            },
          }),
          googleService.updateFileName({
            oauthToken: oauthToken as string,
            file: {
              id: fileId as string,
              name: `${fileName as string}.json`,
            },
          }),
        ]).then(() => {
          dispatch(setDoUpload(false));
          dispatch(setLastUploadedAt(new Date()));
          dispatch(setSavingStatus('saved'));
        });
      } else {
        dispatch(setDoUpload(false));
      }
    }
  }, [doUpload, oauthToken, fileId]);

  return (
    <div className={classNames(className, 'flex overflow-y-hidden')}>
      <ContactList className="w-1/3 px-3 border-r-[1px] border-gray-200 dark:border-gray-700" />
      <ContactDetail className="w-2/3 px-3" />
    </div>
  );
}
