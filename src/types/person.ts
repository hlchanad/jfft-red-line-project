import { nanoid } from 'nanoid';

export interface Person {
  id: string;
  name: string;
  sex: 'M' | 'F';
  skypeId?: string;
  targetId?: string;
  images: Array<{ base64: string }>;
  remarks?: string;
}

export class Person {
  constructor(args: {
    id?: string;
    name?: string;
    sex: 'M' | 'F';
    skypeId?: string;
    targetId?: string;
    remarks?: string;
  }) {
    this.id = args?.id ?? nanoid();
    this.name = args?.name ?? '未命名參加者';
    this.sex = args?.sex;
    this.skypeId = args?.skypeId;
    this.targetId = args?.targetId;
    this.remarks = args?.remarks;
  }
}
