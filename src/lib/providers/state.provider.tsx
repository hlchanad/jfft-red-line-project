import React from 'react';

interface IStateContext<T> {
  state: T | undefined;
  setState: (state: T) => void;
}

export function StateProviderFactory<T>(options?: { defaultState: T }) {
  const Context = React.createContext<IStateContext<T> | undefined>(undefined);

  const Provider = ({ children }) => {
    const [state, setState] = React.useState<T | undefined>(
      options?.defaultState ?? undefined,
    );
    return (
      <Context.Provider value={{ state, setState }}>
        {children}
      </Context.Provider>
    );
  };
  const useState = () => {
    const context = React.useContext(Context);

    if (!context) {
      throw new Error('useState() must be used within a StateProvider');
    }

    return context;
  };

  return { Provider, useState };
}
